import 'package:flutter/material.dart';
import 'package:karani/components/Home/CategoryListComponent.dart';
import 'package:karani/components/home/MainMenuCardComponent.dart';
import 'package:karani/components/home/NavigationDrawerComponent.dart';
import 'package:karani/components/home/CarouselComponent.dart';
import 'package:karani/services/Network.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  Network network = Network();
  @override
  void initState() {
    super.initState();
    _getProfile();
  }

  _getProfile() async {
    await network.fetchProfile();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
        child: NavigationDrawerComponent(),
      ),
      appBar: AppBar(
        title: Text('Karani'),
        actions: <Widget>[
          IconButton(
              icon: Icon(
                Icons.search,
                color: Colors.white,
              ),
              onPressed: null)
        ],
        // leading: IconButton(
        //   icon: Icon(Icons.menu, color: Colors.white),
        //   onPressed: null,
        // ),
      ),
      body: Center(
        child: ListView(
          children: <Widget>[
            Column(
              children: [
                CarouselComponent(),
                SizedBox(height: 30),
                MainMenuCardComponent(),
                SizedBox(height: 30),
                Text(
                  'Categories',
                  style: Theme.of(context).textTheme.headline6,
                ),
                SizedBox(height: 10),
                CategoryListComponent(),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
