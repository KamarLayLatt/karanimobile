import 'package:flutter/material.dart';
import 'package:form_validation/form_validation.dart';
import 'package:karani/services/Network.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final _formKey = GlobalKey<FormState>();
  final RoundedLoadingButtonController _btnController =
      RoundedLoadingButtonController();
  Network network = Network();

  Map<String, String> loginFormData = {
    'phone': '',
    "password": '',
    "device_name": 'iphone',
    "device_type": '2'
  };
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  CircleAvatar(
                      radius: 50,
                      backgroundImage: NetworkImage(
                          'https://picsum.photos/id/1027/200/300')),
                ],
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                child: Form(
                  key: _formKey,
                  child: Column(children: <Widget>[
                    TextFormField(
                      validator: (value) => validatePhone(value),
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        prefixIcon: const Icon(
                          Icons.phone,
                          color: Colors.white,
                        ),
                        border: OutlineInputBorder(
                            borderSide: new BorderSide(color: Colors.white)),
                        // hintText: 'Enter your phone',
                        labelText: 'Phone',

                        // helperText: 'Enter your phone',
                      ),
                      onChanged: (String value) {
                        loginFormData['phone'] = value;
                      },
                    ),
                    SizedBox(height: 20),
                    TextFormField(
                      validator: (value) => validatePassword(value),
                      obscureText: true,
                      decoration: InputDecoration(
                        prefixIcon: const Icon(
                          Icons.security,
                          color: Colors.white,
                        ),
                        border: OutlineInputBorder(
                            borderSide: new BorderSide(color: Colors.white)),
                        // hintText: 'Enter your phone',
                        labelText: 'Password',
                        // helperText: 'Enter your Password',
                      ),
                      onChanged: (String value) {
                        loginFormData['password'] = value;
                      },
                    )
                  ]),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    width: 80,
                    height: 40,
                    child: RoundedLoadingButton(
                      child:
                          Text('Login', style: TextStyle(color: Colors.white)),
                      controller: _btnController,
                      color: Color(0xff100A47),
                      borderRadius: 10,
                      onPressed: () => _login(),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  String validatePhone(value) {
    var validator = Validator(
      validators: [
        RequiredValidator(),
        PhoneNumberValidator(),
      ],
    );
    _btnController.stop();
    return validator.validate(
      context: context,
      label: 'Phone',
      value: value,
    );
  }

  String validatePassword(value) {
    var validator = Validator(
      validators: [RequiredValidator(), MinLengthValidator(length: 8)],
    );
    _btnController.stop();
    return validator.validate(
      context: context,
      label: 'Password',
      value: value,
    );
  }

  Future<void> _login() async {
    if (_formKey.currentState.validate()) {
      network.fetchLogin(loginFormData, context, _btnController).then((value) {
        Navigator.of(context).pushNamed('home');
      });
    }
  }
}
