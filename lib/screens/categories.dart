import 'package:flutter/material.dart';
import 'package:karani/models/Api/CategoryList.dart';
import 'package:karani/screens/category_detail.dart';
import 'package:karani/services/Network.dart';
import 'package:cached_network_image/cached_network_image.dart';

class Categories extends StatefulWidget {
  @override
  _CategoriesState createState() => _CategoriesState();
}

class _CategoriesState extends State<Categories> {
  Network network = Network();

  Future<CategoryList> futureCategoryList;

  @override
  void initState() {
    super.initState();
    futureCategoryList = network.fetchCategoryList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Categories'),
      ),
      body: FutureBuilder<CategoryList>(
          future: futureCategoryList,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return Padding(
                  padding: EdgeInsets.all(10),
                  child: ListView.separated(
                    itemCount: snapshot.data.categories.length,
                    separatorBuilder: (context, index) => SizedBox(height: 20),
                    itemBuilder: (context, index) => Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: InkWell(
                        onTap: () => Navigator.of(context).pushNamed(
                            'category_detail',
                            arguments: snapshot.data.categories[index].slug),
                        child: Stack(
                          children: <Widget>[
                            CachedNetworkImage(
                              imageUrl: snapshot
                                  .data.categories[index].coverPhotoPath,
                              imageBuilder: (context, imageProvider) =>
                                  Container(
                                height: 200,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  image: DecorationImage(
                                    image: imageProvider,
                                    colorFilter: ColorFilter.mode(
                                        Theme.of(context)
                                            .primaryColor
                                            .withOpacity(0.6),
                                        BlendMode.dstATop),
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                              placeholder: (context, url) => Center(
                                  child: CircularProgressIndicator(
                                valueColor:
                                    AlwaysStoppedAnimation<Color>(Colors.white),
                              )),
                              errorWidget: (context, url, error) =>
                                  Icon(Icons.error),
                            ),
                            Container(
                              padding: const EdgeInsets.all(10.0),
                              height: 200,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.end,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    snapshot.data.categories[index].title,
                                    style:
                                        Theme.of(context).textTheme.headline5,
                                  ),
                                  SizedBox(height: 10),
                                  Text(
                                    snapshot.data.categories[index].description,
                                    style:
                                        Theme.of(context).textTheme.subtitle1,
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ));
            } else if (snapshot.hasError) {
              return Text('${snapshot.error}');
            }

            return Center(
              child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
              ),
            );
          }),
    );
  }
}
