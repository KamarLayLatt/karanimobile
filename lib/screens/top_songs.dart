import 'package:flutter/material.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:karani/components/commons/SongListComponent.dart';
import 'package:karani/models/Api/TopSongList.dart';
import 'package:karani/models/Data.dart';
import 'package:karani/services/Network.dart';

class TopSongs extends StatefulWidget {
  @override
  _TopSongsState createState() => _TopSongsState();
}

class _TopSongsState extends State<TopSongs> {
  Network network = Network();
  Future<TopSongList> futureSongList;
  final PagingController<int, Data> _pagingController =
      PagingController(firstPageKey: 1);
  @override
  void initState() {
    _pagingController.addPageRequestListener((pageKey) {
      _fetchPage(pageKey);
    });
    super.initState();
  }

  Future<void> _fetchPage(int pageKey) async {
    try {
      futureSongList = network.fetchTopSongList(pageKey);
      final meta = await futureSongList.then((value) => value.top_songs.meta);
      final newItems =
          await futureSongList.then((value) => value.top_songs.data);
      final isLastPage = meta.currentPage == meta.lastPage;
      if (isLastPage) {
        _pagingController.appendLastPage(newItems);
      } else {
        final nextPageKey = pageKey + 1;
        _pagingController.appendPage(newItems, nextPageKey);
      }
    } catch (error) {
      _pagingController.error = error;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Top Songs'),
      ),
      body: SongListComponent(pagingController: _pagingController),
    );
  }
}
