import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:karani/models/Api/Song.dart';
import 'package:karani/services/Network.dart';

class SongDetail extends StatefulWidget {
  final param, title;

  const SongDetail({Key key, this.param, this.title}) : super(key: key);

  @override
  _SongDetailState createState() => _SongDetailState();
}

class _SongDetailState extends State<SongDetail> {
  Network network = Network();
  Future<Song> futureSong;
  @override
  void initState() {
    super.initState();
    futureSong = network.fetchSong(widget.param);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          widget.title,
          style: Theme.of(context).textTheme.subtitle1,
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Icon(Icons.favorite),
          )
        ],
      ),
      body: FutureBuilder<Song>(
          future: futureSong,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return SingleChildScrollView(
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  child: Column(
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'code: ${snapshot.data.code}',
                            style: Theme.of(context).textTheme.subtitle1,
                          ),
                        ],
                      ),
                      SizedBox(height: 10),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          if (snapshot.data.style != null)
                            Text(
                              'style: ${snapshot.data.style}',
                              style: Theme.of(context).textTheme.subtitle1,
                            ),
                        ],
                      ),
                      SizedBox(height: 10),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          if (snapshot.data.key != null)
                            Text(
                              'key: ${snapshot.data.key}',
                              style: Theme.of(context).textTheme.subtitle1,
                            ),
                        ],
                      ),
                      SizedBox(height: 20),
                      SingleChildScrollView(
                        child: Html(
                          data: snapshot.data.lyrics,
                          style: {
                            'body': Style(
                                fontSize: FontSize(15.0),
                                color: Colors.white70),
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              );
            } else if (snapshot.hasError) {
              return Text("${snapshot.error}");
            }
            return Center(
              child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
              ),
            );
          }),
    );
  }
}
