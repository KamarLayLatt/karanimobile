import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:karani/models/Api/UploadProfilePhoto.dart';
import 'package:karani/services/Network.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';

class EditProfilePhoto extends StatefulWidget {
  @override
  _EditProfilePhotoState createState() => _EditProfilePhotoState();
}

class _EditProfilePhotoState extends State<EditProfilePhoto> {
  File _image;
  final picker = ImagePicker();
  Network network = Network();
  Future<UploadProfilePhoto> uploadProfilePath;
  final RoundedLoadingButtonController _btnController =
      RoundedLoadingButtonController();
  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);
    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
    // print('getImage');
  }

  Future save(BuildContext context) async {
    if (_image != null) {
      await network.fetchUploadProfilePhoto(context, _image, _btnController);
      Navigator.pop(context);
      Navigator.pop(context);
      Navigator.of(context).pushNamed('profile');
    } else {
      _btnController.stop();
      return showDialog(
          context: context,
          builder: (_) => new AlertDialog(
                title: Text("Warning"),
                content: Text('Please Choose Photo'),
                actions: <Widget>[
                  // ignore: deprecated_member_use
                  FlatButton(
                    child: Text('Ok'),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  )
                ],
              ));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Edit Profile Photo'),
      ),
      body: Container(
        padding: EdgeInsets.all(20),
        child: Column(
          children: <Widget>[
            _image != null
                ? CircleAvatar(
                    radius: 70,
                    backgroundImage: FileImage(_image),
                  )
                : CachedNetworkImage(
                    imageUrl: 'https://picsum.photos/id/1027/200/300',
                    imageBuilder: (context, imageProvider) => CircleAvatar(
                        radius: 70, backgroundImage: imageProvider),
                    placeholder: (context, url) => Center(
                        child: CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                    )),
                    errorWidget: (context, url, error) => Icon(Icons.error),
                  ),
            SizedBox(height: 20),
            SizedBox(
              width: double.infinity,
              child: TextButton(
                  style: ButtonStyle(
                    foregroundColor:
                        MaterialStateProperty.all<Color>(Colors.blue),
                    backgroundColor:
                        MaterialStateProperty.all<Color>(Colors.white),
                  ),
                  onPressed: () => getImage(),
                  child: Text('Change Profile Photo')),
            ),
            SizedBox(height: 20),
            SizedBox(
              width: double.infinity,
              child: RoundedLoadingButton(
                child: Text('Save', style: TextStyle(color: Colors.white)),
                controller: _btnController,
                color: Color(0xff100A47),
                borderRadius: 10,
                onPressed: () => save(context),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
