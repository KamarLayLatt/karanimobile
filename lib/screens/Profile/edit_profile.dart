import 'package:flutter/material.dart';
import 'package:form_validation/form_validation.dart';
import 'package:karani/services/Network.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';

class EditProfile extends StatefulWidget {
  @override
  _EditProfileState createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile> {
  final _formKey = GlobalKey<FormState>();
  final RoundedLoadingButtonController _btnController =
      RoundedLoadingButtonController();
  Network network = Network();

  Map<String, String> loginFormData = {
    "name": '',
    "title": '',
    "gender": '',
  };
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Edit Profile'),
        actions: <Widget>[
          TextButton(
              onPressed: () => null,
              child: Text(
                'Save',
                style: Theme.of(context).textTheme.subtitle1,
              ))
        ],
      ),
      body: Container(
        child: Container(
          child: Column(
            // mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                child: Form(
                  key: _formKey,
                  child: Column(children: <Widget>[
                    TextFormField(
                      validator: (value) => validateName(value),
                      // keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        prefixIcon: const Icon(
                          Icons.account_circle,
                          color: Colors.white,
                        ),
                        border: OutlineInputBorder(
                            borderSide: new BorderSide(color: Colors.white)),
                        // hintText: 'Enter your phone',
                        labelText: 'Name',

                        // helperText: 'Enter your phone',
                      ),
                      onChanged: (String value) {
                        loginFormData['name'] = value;
                      },
                    ),
                    SizedBox(height: 20),
                    TextFormField(
                      validator: (value) => validateTitle(value),
                      // obscureText: true,
                      decoration: InputDecoration(
                        prefixIcon: const Icon(
                          Icons.description,
                          color: Colors.white,
                        ),
                        border: OutlineInputBorder(
                            borderSide: new BorderSide(color: Colors.white)),
                        // hintText: 'Enter your phone',
                        labelText: 'Title',
                        // helperText: 'Enter your Password',
                      ),
                      onChanged: (String value) {
                        loginFormData['title'] = value;
                      },
                    )
                  ]),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  // SizedBox(
                  //   width: 80,
                  //   height: 40,
                  //   child: RoundedLoadingButton(
                  //     child:
                  //         Text('Save', style: TextStyle(color: Colors.white)),
                  //     controller: _btnController,
                  //     color: Color(0xff100A47),
                  //     borderRadius: 10,
                  //     onPressed: () => _login(),
                  //   ),
                  // ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  String validateName(value) {
    var validator = Validator(
      validators: [
        RequiredValidator(),
        PhoneNumberValidator(),
      ],
    );
    _btnController.stop();
    return validator.validate(
      context: context,
      label: 'Phone',
      value: value,
    );
  }

  String validateTitle(value) {
    var validator = Validator(
      validators: [RequiredValidator(), MinLengthValidator(length: 8)],
    );
    _btnController.stop();
    return validator.validate(
      context: context,
      label: 'Password',
      value: value,
    );
  }

  String validateGender(value) {
    var validator = Validator(
      validators: [RequiredValidator(), MinLengthValidator(length: 8)],
    );
    _btnController.stop();
    return validator.validate(
      context: context,
      label: 'Password',
      value: value,
    );
  }

  Future<void> _login() async {
    if (_formKey.currentState.validate()) {
      network.fetchLogin(loginFormData, context, _btnController).then((value) {
        Navigator.of(context).pushNamed('home');
      });
    }
  }
}
