import 'package:flutter/material.dart';
import 'package:karani/components/profile/FavoriteList.dart';
import 'package:karani/components/profile/ProfileHeader.dart';

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Profile')),
      body: Container(
        padding: EdgeInsets.all(20),
        child: Column(
          children: <Widget>[
            ProfileHeader(),
            Divider(
              color: Colors.white38,
            ),
            FavoriteList()
          ],
        ),
      ),
    );
  }
}
