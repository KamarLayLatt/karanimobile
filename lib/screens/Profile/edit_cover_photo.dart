import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class EditCoverPhoto extends StatefulWidget {
  @override
  _EditCoverPhotoState createState() => _EditCoverPhotoState();
}

class _EditCoverPhotoState extends State<EditCoverPhoto> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Edit Cover Photo'),
        actions: <Widget>[
          TextButton(
              onPressed: () => null,
              child: Text(
                'Save',
                style: Theme.of(context).textTheme.subtitle1,
              ))
        ],
      ),
      body: Container(
        padding: EdgeInsets.all(20),
        child: Column(
          children: <Widget>[
            CachedNetworkImage(
              imageUrl: 'https://picsum.photos/id/1018/200/300',
              imageBuilder: (context, imageProvider) => Container(
                height: MediaQuery.of(context).size.height / 4,
                width: double.infinity,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  image: DecorationImage(
                    image: imageProvider,
                    colorFilter: ColorFilter.mode(
                        Theme.of(context).primaryColor.withOpacity(0.6),
                        BlendMode.dstATop),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              placeholder: (context, url) => CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
              ),
              errorWidget: (context, url, error) => Icon(Icons.error),
            ),
            SizedBox(height: 20),
            SizedBox(
              width: double.infinity,
              child: TextButton(
                  style: ButtonStyle(
                    foregroundColor:
                        MaterialStateProperty.all<Color>(Colors.blue),
                    backgroundColor:
                        MaterialStateProperty.all<Color>(Colors.white),
                  ),
                  onPressed: () =>
                      Navigator.of(context).pushNamed('edit_profile'),
                  child: Text('Change Cover Photo')),
            )
          ],
        ),
      ),
    );
  }
}
