import 'package:flutter/material.dart';
import 'package:karani/components/commons/SongListComponent.dart';
import 'package:karani/models/Api/CategorySongList.dart';
import 'package:karani/models/Category.dart';
import 'package:karani/services/Network.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:karani/models/Data.dart';
import 'package:cached_network_image/cached_network_image.dart';

class CategoryDetail extends StatefulWidget {
  final param;
  CategoryDetail({Key key, this.param}) : super(key: key);

  @override
  _CategoryDetailState createState() => _CategoryDetailState();
}

class _CategoryDetailState extends State<CategoryDetail> {
  Network network = Network();
  Future<CategorySongList> futureSongList;
  final PagingController<int, Data> _pagingController =
      PagingController(firstPageKey: 1);
  Future<CategorySongList> futureCategory;
  @override
  void initState() {
    futureCategory = network.fetchCategorySongList(widget.param, 1);
    _pagingController.addPageRequestListener((pageKey) {
      _fetchPage(pageKey);
    });
    super.initState();
  }

  Future<void> _fetchPage(int pageKey) async {
    try {
      futureSongList = network.fetchCategorySongList(widget.param, pageKey);
      final meta = await futureSongList.then((value) => value.songs.meta);
      final newItems = await futureSongList.then((value) => value.songs.data);
      final isLastPage = meta.currentPage == meta.lastPage;

      if (isLastPage) {
        _pagingController.appendLastPage(newItems);
      } else {
        final nextPageKey = pageKey + 1;
        _pagingController.appendPage(newItems, nextPageKey);
      }
    } catch (error) {
      _pagingController.error = error;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: NestedScrollView(
            // Setting floatHeaderSlivers to true is required in order to float
            // the outer slivers over the inner scrollable.
            floatHeaderSlivers: true,
            headerSliverBuilder:
                (BuildContext context, bool innerBoxIsScrolled) {
              return <Widget>[
                SliverAppBar(
                  // title: Text("Category Detail"),
                  pinned: true,
                  // floating: true,
                  // stretch: true,
                  expandedHeight: 220.0,
                  flexibleSpace: FutureBuilder<CategorySongList>(
                      future: futureCategory,
                      builder: (context, snapshot) {
                        if (snapshot.hasData) {
                          return FlexibleSpaceBar(
                            centerTitle: false,
                            // stretchModes: <StretchMode>[
                            //   StretchMode.zoomBackground,
                            //   StretchMode.blurBackground,
                            //   StretchMode.fadeTitle
                            // ],
                            title: Text(snapshot.data.category.title,
                                style: Theme.of(context).textTheme.subtitle1),
                            background: CachedNetworkImage(
                              imageUrl: snapshot.data.category.coverPhotoPath,
                              fit: BoxFit.cover,
                              placeholder: (context, url) => Center(
                                  child: CircularProgressIndicator(
                                valueColor:
                                    AlwaysStoppedAnimation<Color>(Colors.white),
                              )),
                              errorWidget: (context, url, error) =>
                                  Icon(Icons.error),
                            ),
                          );
                        } else if (snapshot.hasError) {
                          return Text("${snapshot.error}");
                        }
                        return Center(
                          child: CircularProgressIndicator(
                            valueColor:
                                AlwaysStoppedAnimation<Color>(Colors.white),
                          ),
                        );
                      }),
                ),
              ];
            },
            body: SongListComponent(pagingController: _pagingController)));
  }
}
