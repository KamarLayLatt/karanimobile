import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:karani/models/Api/AllSongList.dart';
import 'package:karani/models/Api/BannerList.dart';
import 'package:karani/models/Api/CategoryList.dart';
import 'package:karani/models/Api/CategorySongList.dart';
import 'package:karani/models/Api/FavoriteSongList.dart';
import 'package:karani/models/Api/Login.dart';
import 'package:karani/models/Api/Profile.dart';
import 'package:karani/models/Api/Song.dart';
import 'package:karani/models/Api/TopSongList.dart';
import 'package:karani/models/Api/UploadProfilePhoto.dart';
// import 'package:karani/models/Post.dart';
import 'package:karani/services/Api.dart';
import 'package:karani/services/Service.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';
import 'package:dio/dio.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Network {
  Api api = Api();
  Service service = Service();
  var dio = Dio();
  _showError(BuildContext context, http.Response response) {
    var data = jsonDecode(response.body);
    return showDialog(
        context: context,
        builder: (_) => new AlertDialog(
              title: Text("Error"),
              content: Text(data['message']),
              actions: <Widget>[
                // ignore: deprecated_member_use
                FlatButton(
                  child: Text('Ok'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            ));
  }

  // Future<List<Post>> fetchPost() async {
  //   final response = await http.get(Uri.parse(api.post()));
  //   List data = (jsonDecode(response.body) as List)
  //       .map((i) => Post.fromJson(i))
  //       .toList();
  //   return _responseFormat(response, data);
  // }

  Future<Login> fetchLogin(Map<String, String> bodyData, BuildContext context,
      RoundedLoadingButtonController btnController) async {
    final response = await http.post(
      Uri.parse(api.login()),
      headers: <String, String>{'Accept': 'application/json'},
      body: bodyData,
    );
    btnController.stop();
    try {
      var data = Login.fromJson(jsonDecode(response.body)['data']);
      service.storeAuthData(data.token);
      return data;
    } catch (e) {
      return _showError(context, response);
    }
  }

  Future<CategoryList> fetchCategoryList() async {
    final response = await http.get(
      Uri.parse(api.categoryList()),
      headers: <String, String>{
        'Accept': 'application/json',
        'Authorization': 'Bearer 1|S93gfZpwI9xLl9nhIt1kZ7qXqdoW0TZoIhgfcreQ'
      },
    );
    var data = CategoryList.fromJson(jsonDecode(response.body)['data']);
    return data;
  }

  Future<BannerList> fetchBannerList() async {
    final response = await http.get(
      Uri.parse(api.bannerList()),
      headers: <String, String>{
        'Accept': 'application/json',
        'Authorization': 'Bearer 1|S93gfZpwI9xLl9nhIt1kZ7qXqdoW0TZoIhgfcreQ'
      },
    );
    var data = BannerList.fromJson(jsonDecode(response.body)['data']);
    return data;
  }

  Future<AllSongList> fetchAllSongList(int pageKey) async {
    final response = await http.get(
      Uri.parse(api.allSongList(pageKey)),
      headers: <String, String>{
        'Accept': 'application/json',
        'Authorization': 'Bearer 1|S93gfZpwI9xLl9nhIt1kZ7qXqdoW0TZoIhgfcreQ'
      },
    );
    // print(response.body);
    var data = AllSongList.fromJson(jsonDecode(response.body)['data']);
    return data;
  }

  Future<TopSongList> fetchTopSongList(int pageKey) async {
    final response = await http.get(
      Uri.parse(api.topSongList(pageKey)),
      headers: <String, String>{
        'Accept': 'application/json',
        'Authorization': 'Bearer 1|S93gfZpwI9xLl9nhIt1kZ7qXqdoW0TZoIhgfcreQ'
      },
    );
    // print(response.body);
    var data = TopSongList.fromJson(jsonDecode(response.body)['data']);
    return data;
  }

  Future<FavoriteSongList> fetchFavoriteSongList(int pageKey) async {
    final response = await http.get(
      Uri.parse(api.favoriteSongList(pageKey)),
      headers: <String, String>{
        'Accept': 'application/json',
        'Authorization': 'Bearer 1|S93gfZpwI9xLl9nhIt1kZ7qXqdoW0TZoIhgfcreQ'
      },
    );
    // print(response.body);
    var data = FavoriteSongList.fromJson(jsonDecode(response.body)['data']);
    return data;
  }

  Future<CategorySongList> fetchCategorySongList(
      String slug, int pageKey) async {
    final response = await http.get(
      Uri.parse(api.categorySongList(slug, pageKey)),
      headers: <String, String>{
        'Accept': 'application/json',
        'Authorization': 'Bearer 1|S93gfZpwI9xLl9nhIt1kZ7qXqdoW0TZoIhgfcreQ'
      },
    );
    // print(response.body);
    var data = CategorySongList.fromJson(jsonDecode(response.body)['data']);
    return data;
  }

  Future<Song> fetchSong(String slug) async {
    final response = await http.get(
      Uri.parse(api.songDetail(slug)),
      headers: <String, String>{
        'Accept': 'application/json',
        'Authorization': 'Bearer 1|S93gfZpwI9xLl9nhIt1kZ7qXqdoW0TZoIhgfcreQ'
      },
    );
    // print(response.body);
    var data = Song.fromJson(jsonDecode(response.body)['data']);
    return data;
  }

  // ignore: missing_return
  Future<UploadProfilePhoto> fetchUploadProfilePhoto(BuildContext context,
      File file, RoundedLoadingButtonController btnController) async {
    var formData = FormData.fromMap({
      'profile': await MultipartFile.fromFile(file.path,
          filename: file.path.split('/').last)
    });
    try {
      dio.options.headers['Accept'] = 'application/json';
      dio.options.headers["authorization"] =
          "Bearer 1|S93gfZpwI9xLl9nhIt1kZ7qXqdoW0TZoIhgfcreQ";
      await dio.post(api.uploadProfilePhoto(), data: formData);
    } on DioError catch (e) {
      var data = e.response.data;
      return showDialog(
          context: context,
          builder: (_) => new AlertDialog(
                title: Text("Error"),
                content: Text(data['message']),
                actions: <Widget>[
                  // ignore: deprecated_member_use
                  FlatButton(
                    child: Text('Ok'),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  )
                ],
              ));
    }
    await fetchProfile();
    btnController.stop();
  }

  // ignore: missing_return
  Future<Profile> fetchProfile() async {
    final response = await http.get(
      Uri.parse(api.profile()),
      headers: <String, String>{
        'Accept': 'application/json',
        'Authorization': 'Bearer 1|S93gfZpwI9xLl9nhIt1kZ7qXqdoW0TZoIhgfcreQ'
      },
    );
    service.setUser(jsonDecode(response.body)['data']);
  }
}
