// import 'package:flutter/cupertino.dart';

import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:karani/models/Api/Profile.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Service {
  storeAuthData(data) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString("token", data);
  }

  Future<String> getAuthToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString("token");
  }

  setUser(data) async {
    String user = jsonEncode(Profile.fromJson(data));
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString("profile", user);
  }

  Future<Profile> getUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    Map sharedProfile = jsonDecode(prefs.getString('profile'));
    return Profile.fromJson(sharedProfile);
  }
}
