class Api {
  String baseUrl = "https://karani.org/api";
  String login() => '$baseUrl/login';
  String categoryList() => '$baseUrl/categories';
  String bannerList() => '$baseUrl/banners';
  String allSongList(int pageKey) => '$baseUrl/songs?page=$pageKey';
  String topSongList(int pageKey) => '$baseUrl/songs/top/list?page=$pageKey';
  String favoriteSongList(int pageKey) => '$baseUrl/favorites?page=$pageKey';
  String categorySongList(String slug, int pageKey) =>
      '$baseUrl/categories/$slug?page=$pageKey';
  String songDetail(String slug) => '$baseUrl/songs/$slug';
  String profile() => '$baseUrl/profile';
  String uploadProfilePhoto() => '$baseUrl/profile/upload-profile';
}
