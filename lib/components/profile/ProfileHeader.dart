import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:karani/models/Api/Profile.dart';
import 'package:karani/services/Service.dart';
import 'package:shimmer/shimmer.dart';

// ignore: must_be_immutable
class ProfileHeader extends StatefulWidget {
  @override
  _ProfileHeaderState createState() => _ProfileHeaderState();
}

class _ProfileHeaderState extends State<ProfileHeader> {
  Service service = Service();

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<Profile>(
        future: service.getUser(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Stack(
              children: <Widget>[
                BackgroundPhoto(snapshot.data),
                ProfilePhoto(snapshot.data),
              ],
            );
          } else if (snapshot.hasError) {
            return Text("${snapshot.error}");
          }

          return Container(
            padding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
            child: Shimmer.fromColors(
              baseColor: Theme.of(context).primaryColor,
              highlightColor: Colors.grey[100],
              // enabled: true,
              child: SizedBox(
                height: 500,
                child: Container(
                  width: double.infinity,
                  decoration: BoxDecoration(
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          );

          // return Center(
          //   child: CircularProgressIndicator(
          //     valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
          //   ),
          // );
        });
  }
}

// ignore: must_be_immutable
class BackgroundPhoto extends StatelessWidget {
  Service service = Service();
  Profile user;
  BackgroundPhoto(this.user);

  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      imageUrl: user.coverPhotoPath,
      imageBuilder: (context, imageProvider) => Stack(
        children: <Widget>[
          Container(
            height: MediaQuery.of(context).size.height / 4,
            width: double.infinity,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              image: DecorationImage(
                image: imageProvider,
                colorFilter: ColorFilter.mode(
                    Theme.of(context).primaryColor.withOpacity(0.6),
                    BlendMode.dstATop),
                fit: BoxFit.cover,
              ),
            ),
          ),
          Container(
            height: MediaQuery.of(context).size.height / 4,
            width: double.infinity,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                IconButton(
                    icon: Icon(
                      Icons.camera_alt,
                      color: Colors.white,
                    ),
                    onPressed: () =>
                        Navigator.of(context).pushNamed('edit_cover_photo'))
              ],
            ),
          ),
        ],
      ),
      placeholder: (context, url) => Container(
        child: Shimmer.fromColors(
          baseColor: Theme.of(context).primaryColor,
          highlightColor: Colors.grey[100],
          // enabled: true,
          child: SizedBox(
            height: MediaQuery.of(context).size.height / 4,
            width: double.infinity,
            child: Container(
              width: double.infinity,
              decoration: BoxDecoration(
                color: Colors.white,
              ),
            ),
          ),
        ),
      ),
      errorWidget: (context, url, error) => Icon(Icons.error),
    );
  }
}

// ignore: must_be_immutable
class ProfilePhoto extends StatefulWidget {
  Profile user;

  ProfilePhoto(this.user);

  @override
  _ProfilePhotoState createState() => _ProfilePhotoState();
}

class _ProfilePhotoState extends State<ProfilePhoto> {
  Service service = Service();

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Stack(
          children: <Widget>[
            Container(
              alignment: Alignment.bottomCenter,
              height: MediaQuery.of(context).size.height / 4 + 50,
              width: double.infinity,
              child: CachedNetworkImage(
                imageUrl: widget.user.profilePath,
                imageBuilder: (context, imageProvider) =>
                    CircleAvatar(radius: 70, backgroundImage: imageProvider),
                placeholder: (context, url) => Container(
                  child: Shimmer.fromColors(
                    baseColor: Theme.of(context).primaryColor,
                    highlightColor: Colors.grey[100],
                    // enabled: true,
                    child: CircleAvatar(
                      radius: 70,
                    ),
                  ),
                ),
                errorWidget: (context, url, error) => Icon(Icons.error),
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 100),
              alignment: Alignment.bottomCenter,
              height: MediaQuery.of(context).size.height / 4 + 50,
              width: double.infinity,
              child: IconButton(
                icon: Icon(
                  Icons.camera_alt,
                  color: Colors.white,
                ),
                onPressed: () =>
                    Navigator.of(context).pushNamed('edit_profile_photo'),
              ),
            )
          ],
        ),
        SizedBox(height: 10),
        Text(
          widget.user.name,
          style: Theme.of(context).textTheme.headline6,
        ),
        SizedBox(height: 10),
        Text(
          widget.user.title,
          style: Theme.of(context).textTheme.overline,
        ),
        SizedBox(height: 10),
        SizedBox(
          width: double.infinity,
          child: TextButton(
              style: ButtonStyle(
                foregroundColor: MaterialStateProperty.all<Color>(Colors.blue),
                backgroundColor: MaterialStateProperty.all<Color>(Colors.white),
              ),
              onPressed: () => Navigator.of(context).pushNamed('edit_profile'),
              child: Text('Edit Profile')),
        )
      ],
    );
  }
}
