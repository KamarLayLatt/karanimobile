import 'package:flutter/material.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:karani/components/commons/SongListComponent.dart';
import 'package:karani/models/Api/FavoriteSongList.dart';
import 'package:karani/models/Data.dart';
import 'package:karani/services/Network.dart';

class FavoriteList extends StatefulWidget {
  @override
  _FavoriteListState createState() => _FavoriteListState();
}

class _FavoriteListState extends State<FavoriteList> {
  Network network = Network();
  Future<FavoriteSongList> futureSongList;
  final PagingController<int, Data> _pagingController =
      PagingController(firstPageKey: 1);
  @override
  void initState() {
    _pagingController.addPageRequestListener((pageKey) {
      _fetchPage(pageKey);
    });
    super.initState();
  }

  Future<void> _fetchPage(int pageKey) async {
    try {
      futureSongList = network.fetchFavoriteSongList(pageKey);
      final meta =
          await futureSongList.then((value) => value.favorite_songs.meta);
      final newItems =
          await futureSongList.then((value) => value.favorite_songs.data);
      final isLastPage = meta.currentPage == meta.lastPage;
      if (isLastPage) {
        _pagingController.appendLastPage(newItems);
      } else {
        final nextPageKey = pageKey + 1;
        _pagingController.appendPage(newItems, nextPageKey);
      }
    } catch (error) {
      _pagingController.error = error;
    }
  }

  @override
  Widget build(BuildContext context) {
    // return Column(
    //   children: <Widget>[
    //     Text('Favorite Lists'),
    //     Expanded(child: SongListComponent(pagingController: _pagingController))
    //   ],
    // );
    return Expanded(
        child: SongListComponent(pagingController: _pagingController));
  }
}
