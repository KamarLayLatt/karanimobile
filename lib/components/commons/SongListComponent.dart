import 'package:flutter/material.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:karani/models/Api/AllSongList.dart';
import 'package:karani/models/Data.dart';
import 'package:karani/services/Network.dart';
import 'package:shimmer/shimmer.dart';

class SongListComponent extends StatefulWidget {
  final PagingController<int, Data> pagingController;

  const SongListComponent({Key key, this.pagingController}) : super(key: key);
  @override
  _SongListComponentState createState() => _SongListComponentState();
}

class _SongListComponentState extends State<SongListComponent> {
  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: () => Future.sync(
        () => widget.pagingController.refresh(),
      ),
      child: PagedListView<int, Data>.separated(
        pagingController: widget.pagingController,
        builderDelegate: PagedChildBuilderDelegate<Data>(
          itemBuilder: (context, item, index) => Container(
            padding: EdgeInsets.all(10),
            child: SongListCard(item, index),
          ),
          firstPageProgressIndicatorBuilder: (_) => Center(
            child: CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
            ),
          ),
          newPageProgressIndicatorBuilder: (_) => Center(
            child: CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
            ),
          ),
        ),
        separatorBuilder: (context, index) => SizedBox(height: 10),
      ),
    );
  }

  @override
  void dispose() {
    widget.pagingController.dispose();
    super.dispose();
  }
}

class SongListCard extends StatelessWidget {
  final Data item;
  final int index;
  SongListCard(this.item, this.index);
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => Navigator.of(context).pushNamed('song_detail',
          arguments: SongDetailArguments(item.slug, item.title)),
      child: Container(
        padding: EdgeInsets.all(5),
        decoration: BoxDecoration(
            color: Theme.of(context).primaryColor,
            borderRadius: BorderRadius.circular(10)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              // 'Praise & Worship',
              '${item.categories.map((val) => val.title).join(', ')}',
              style: Theme.of(context).textTheme.caption,
            ),
            Text(
              '${item.title}',
              style: Theme.of(context).textTheme.subtitle1,
            ),
            Row(
              children: <Widget>[
                // Icon(Icons.queue_music),
                SizedBox(width: 10),
                Expanded(
                  child: Text(
                    '${item.lyrics}',
                    overflow: TextOverflow.ellipsis,
                    maxLines: 3,
                    softWrap: false,
                    style: TextStyle(color: Colors.white54, fontSize: 12),
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class SongDetailArguments {
  final String slug;
  final String title;

  SongDetailArguments(this.slug, this.title);
}
