import 'package:flutter/material.dart';

class MainMenuCardComponent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        MenuCard(
          icon: Icons.music_video_sharp,
          title: 'All Songs',
          routeName: 'all_songs',
        ),
        MenuCard(
          icon: Icons.favorite,
          title: 'Top Songs',
          routeName: 'top_songs',
        ),
      ],
    );
  }
}

class MenuCard extends StatelessWidget {
  final IconData icon;
  final String title;
  final String routeName;

  const MenuCard({Key key, this.icon, this.title, this.routeName})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.of(context).pushNamed(routeName);
      },
      child: Container(
        height: 120,
        width: 120,
        decoration: BoxDecoration(
          color: Theme.of(context).accentColor,
          borderRadius: BorderRadius.circular(10),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(icon, color: Colors.white, size: 50),
            SizedBox(height: 10),
            Text('$title', style: Theme.of(context).textTheme.bodyText1),
          ],
        ),
      ),
    );
  }
}
