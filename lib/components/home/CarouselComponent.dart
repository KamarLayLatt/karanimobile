import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:karani/models/Api/BannerList.dart';
import 'package:karani/services/Network.dart';
import 'package:shimmer/shimmer.dart';
import 'package:cached_network_image/cached_network_image.dart';

class CarouselComponent extends StatefulWidget {
  @override
  _CarouselComponentState createState() => _CarouselComponentState();
}

class _CarouselComponentState extends State<CarouselComponent> {
  Network network = Network();
  Future<BannerList> futureBannerList;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    futureBannerList = network.fetchBannerList();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<BannerList>(
        future: futureBannerList,
        builder: (context, snapshot) {
          // var categories = snapshot.data.banners;
          if (snapshot.hasData) {
            return Padding(
              padding: const EdgeInsets.symmetric(horizontal: 0, vertical: 10),
              child: CarouselSlider(
                options: CarouselOptions(
                  height: 200.0,
                  autoPlay: true,
                  autoPlayInterval: Duration(seconds: 10),
                  enlargeCenterPage: true,
                ),
                items: snapshot.data.banners.map((index) {
                  return Builder(
                    builder: (BuildContext context) {
                      return Container(
                        width: MediaQuery.of(context).size.width,
                        margin: EdgeInsets.symmetric(horizontal: 2.0),
                        child: CachedNetworkImage(
                          imageUrl: index.imagePath,
                          imageBuilder: (context, imageProvider) => Container(
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image: imageProvider,
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                          placeholder: (context, url) => Center(
                              child: CircularProgressIndicator(
                            valueColor:
                                AlwaysStoppedAnimation<Color>(Colors.white),
                          )),
                          errorWidget: (context, url, error) =>
                              Icon(Icons.error),
                        ),
                      );
                    },
                  );
                }).toList(),
              ),
            );
          } else if (snapshot.hasError) {
            return Text('${snapshot.error}');
          }
          return Container(
            padding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
            child: Shimmer.fromColors(
              baseColor: Theme.of(context).primaryColor,
              highlightColor: Colors.grey[100],
              // enabled: true,
              child: Container(
                height: 200,
                decoration: BoxDecoration(color: Colors.white),
              ),
            ),
          );
          // return CircularProgressIndicator();
        });
  }
}
