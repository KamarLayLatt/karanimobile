import 'package:flutter/material.dart';
import 'package:karani/models/Api/CategoryList.dart';
import 'package:karani/services/Network.dart';
import 'package:shimmer/shimmer.dart';
import 'package:cached_network_image/cached_network_image.dart';

class CategoryListComponent extends StatefulWidget {
  @override
  _CategoryListComponentState createState() => _CategoryListComponentState();
}

class _CategoryListComponentState extends State<CategoryListComponent> {
  Network network = Network();
  Future<CategoryList> futureCategoryList;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    futureCategoryList = network.fetchCategoryList();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<CategoryList>(
        future: futureCategoryList,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Container(
                height: 180,
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
                child: ListView.separated(
                  scrollDirection: Axis.horizontal,
                  separatorBuilder: (context, index) => SizedBox(
                    width: 15,
                  ),
                  itemCount: snapshot.data.categories.length,
                  itemBuilder: (context, index) => InkWell(
                    onTap: () => Navigator.of(context).pushNamed(
                        'category_detail',
                        arguments: snapshot.data.categories[index].slug),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width / 2,
                          height: 100,
                          child: CachedNetworkImage(
                            imageUrl:
                                snapshot.data.categories[index].coverPhotoPath,
                            imageBuilder: (context, imageProvider) => Container(
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                  image: imageProvider,
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                            placeholder: (context, url) => Center(
                                child: CircularProgressIndicator(
                              valueColor:
                                  AlwaysStoppedAnimation<Color>(Colors.white),
                            )),
                            errorWidget: (context, url, error) =>
                                Icon(Icons.error),
                          ),
                        ),
                        SizedBox(height: 10),
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 3.0),
                          width: MediaQuery.of(context).size.width / 2,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text(snapshot.data.categories[index].title,
                                  overflow: TextOverflow.ellipsis,
                                  style: Theme.of(context).textTheme.bodyText1),
                              Container(
                                  height: 15,
                                  width: 15,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(30)),
                                  child: Icon(
                                    Icons.arrow_right_alt_outlined,
                                    color: Theme.of(context).primaryColor,
                                    size: 15,
                                  ))
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ));
          } else if (snapshot.hasError) {
            return Text('${snapshot.error}');
          }
          return Container(
            padding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
            child: Shimmer.fromColors(
              baseColor: Theme.of(context).primaryColor,
              highlightColor: Colors.grey[100],
              // enabled: true,
              child: SizedBox(
                height: 120,
                child: ListView.separated(
                  scrollDirection: Axis.horizontal,
                  separatorBuilder: (context, index) => SizedBox(
                    width: 15,
                  ),
                  itemCount: 4,
                  itemBuilder: (context, index) => Container(
                    width: MediaQuery.of(context).size.width / 2,
                    decoration: BoxDecoration(
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ),
          );
          // return CircularProgressIndicator();
        });
  }
}
