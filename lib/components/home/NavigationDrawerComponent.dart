import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';

class NavigationDrawerComponent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Theme.of(context).accentColor,
      child: ListView(children: <Widget>[
        DrawerHeader(
          decoration: BoxDecoration(
            color: Theme.of(context).primaryColor,
          ),
          child: Center(
            //https://picsum.photos/id/1027/200/300
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  InkWell(
                    onTap: () {
                      Navigator.pop(context);
                      Navigator.of(context).pushNamed('profile');
                    },
                    // child: CircleAvatar(
                    //     radius: 50,
                    //     backgroundImage: NetworkImage(
                    //         'https://picsum.photos/id/1027/200/300')),
                    child: CachedNetworkImage(
                      imageUrl: 'https://picsum.photos/id/1027/200/300',
                      imageBuilder: (context, imageProvider) => CircleAvatar(
                          radius: 50, backgroundImage: imageProvider),
                      placeholder: (context, url) => Center(
                          child: CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                      )),
                      errorWidget: (context, url, error) => Icon(Icons.error),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      'Kamar Lay Latt',
                      style: Theme.of(context).textTheme.subtitle1,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        MenuList(
          icon: Icons.category,
          title: 'Category',
          route: 'categories',
        ),
        MenuList(
          icon: Icons.music_note,
          title: 'Songs',
          route: 'all_songs',
        ),
        MenuList(
          icon: Icons.arrow_upward,
          title: 'Top Songs',
          route: 'top_songs',
        ),
        MenuList(
          icon: Icons.info,
          title: 'About',
          route: 'top_songs',
        ),
      ]),
    );
  }
}

class MenuList extends StatelessWidget {
  final title, route;
  final IconData icon;

  const MenuList({Key key, this.title, this.route, this.icon})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Icon(icon),
      title: Text(title),
      onTap: () {
        Navigator.pop(context);
        Navigator.of(context).pushNamed(route);
      },
    );
  }
}
