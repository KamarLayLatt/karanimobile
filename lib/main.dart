import 'package:flutter/material.dart';
import 'package:karani/route_generator.dart';

void main() {
  runApp(MyApp());
}

final ThemeData _appTheme = _buildAppTheme();

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: _appTheme,
      initialRoute: 'home',
      onGenerateRoute: RouteGenerator.generateRoute,
    );
  }
}

ThemeData _buildAppTheme() {
  final ThemeData base = ThemeData.dark();

  return base.copyWith(
    primaryColor: Color(0xff0E0A33),
    accentColor: Color(0xff0E0A33),
    scaffoldBackgroundColor: Color(0xff080523),
    textTheme: _appTextTheme(base.textTheme),
  );
}

TextTheme _appTextTheme(TextTheme base) {
  return base
      .copyWith(
        bodyText2: base.bodyText2.copyWith(
          fontSize: 18.0,
        ),
      )
      .apply(displayColor: Colors.white, bodyColor: Colors.white);
}
