import 'package:flutter/material.dart';
import 'package:karani/components/commons/SongListComponent.dart';
import 'package:karani/screens/Profile/edit_cover_photo.dart';
import 'package:karani/screens/Profile/edit_profile_photo.dart';
import 'package:karani/screens/all_songs.dart';
import 'package:karani/screens/auth/login.dart';
import 'package:karani/screens/categories.dart';
import 'package:karani/screens/category_detail.dart';
import 'package:karani/screens/Profile/edit_profile.dart';
import 'package:karani/screens/home.dart';
import 'package:karani/screens/Profile/profile.dart';
import 'package:karani/screens/song_detail.dart';
import 'package:karani/screens/top_songs.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    final args = settings.arguments;

    switch (settings.name) {
      case 'login':
        return MaterialPageRoute(builder: (_) => Login());
      case 'home':
        return MaterialPageRoute(builder: (_) => Home());
      case 'profile':
        return MaterialPageRoute(builder: (_) => Profile());
      case 'edit_profile':
        return MaterialPageRoute(builder: (_) => EditProfile());
      case 'edit_profile_photo':
        return MaterialPageRoute(builder: (_) => EditProfilePhoto());
      case 'edit_cover_photo':
        return MaterialPageRoute(builder: (_) => EditCoverPhoto());
      case 'all_songs':
        return MaterialPageRoute(builder: (_) => AllSongs());
      case 'top_songs':
        return MaterialPageRoute(builder: (_) => TopSongs());
      case 'categories':
        return MaterialPageRoute(builder: (_) => Categories());
      case 'category_detail':
        return MaterialPageRoute(builder: (_) => CategoryDetail(param: args));
      case 'song_detail':
        SongDetailArguments songDetailArguments = args;
        return MaterialPageRoute(
            builder: (_) => SongDetail(
                param: songDetailArguments.slug,
                title: songDetailArguments.title));
      default:
        return _errorRoute();
    }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: AppBar(
          title: Text('Error'),
        ),
        body: Center(
          child: Text('Error'),
        ),
      );
    });
  }
}
