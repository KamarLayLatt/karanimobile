class Category {
  int id;
  String title;
  String slug;
  String description;
  String coverPhotoPath;

  Category(
      {this.id, this.title, this.slug, this.description, this.coverPhotoPath});

  Category.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    slug = json['slug'];
    description = json['description'];
    coverPhotoPath = json['cover_photo_path'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['slug'] = this.slug;
    data['description'] = this.description;
    data['cover_photo_path'] = this.coverPhotoPath;
    return data;
  }
}
