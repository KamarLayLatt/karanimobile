class Song {
  int id;
  String code;
  String category;
  String title;
  String slug;
  String youtube;
  String description;
  String songWriter;
  String style;
  String key;
  String musicNotes;
  UploadedBy uploadedBy;
  String lyrics;
  int isFavorite;

  Song(
      {this.id,
      this.code,
      this.category,
      this.title,
      this.slug,
      this.youtube,
      this.description,
      this.songWriter,
      this.style,
      this.key,
      this.musicNotes,
      this.uploadedBy,
      this.lyrics,
      this.isFavorite});

  Song.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    code = json['code'];
    category = json['category'];
    title = json['title'];
    slug = json['slug'];
    youtube = json['youtube'];
    description = json['description'];
    songWriter = json['song_writer'];
    style = json['style'];
    key = json['key'];
    musicNotes = json['music_notes'];
    uploadedBy = json['uploaded_by'] != null
        ? new UploadedBy.fromJson(json['uploaded_by'])
        : null;
    lyrics = json['lyrics'];
    isFavorite = json['is_favorite'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['code'] = this.code;
    data['category'] = this.category;
    data['title'] = this.title;
    data['slug'] = this.slug;
    data['youtube'] = this.youtube;
    data['description'] = this.description;
    data['song_writer'] = this.songWriter;
    data['style'] = this.style;
    data['key'] = this.key;
    data['music_notes'] = this.musicNotes;
    if (this.uploadedBy != null) {
      data['uploaded_by'] = this.uploadedBy.toJson();
    }
    data['lyrics'] = this.lyrics;
    data['is_favorite'] = this.isFavorite;
    return data;
  }
}

class UploadedBy {
  int id;
  String name;

  UploadedBy({this.id, this.name});

  UploadedBy.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    return data;
  }
}
