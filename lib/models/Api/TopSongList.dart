import 'package:karani/models/SongList.dart';

class TopSongList {
  // ignore: non_constant_identifier_names
  SongList top_songs;

  // ignore: non_constant_identifier_names
  TopSongList({this.top_songs});

  TopSongList.fromJson(Map<String, dynamic> json) {
    top_songs = json['top_songs'] != null
        ? new SongList.fromJson(json['top_songs'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.top_songs != null) {
      data['top_songs'] = this.top_songs.toJson();
    }
    return data;
  }
}
