class UploadProfilePhoto {
  String profilePath;

  UploadProfilePhoto({this.profilePath});

  UploadProfilePhoto.fromJson(Map<String, dynamic> json) {
    profilePath = json['profile_path'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['profile_path'] = this.profilePath;
    return data;
  }
}
