import 'package:karani/models/SongList.dart';

class FavoriteSongList {
  // ignore: non_constant_identifier_names
  SongList favorite_songs;

  // ignore: non_constant_identifier_names
  FavoriteSongList({this.favorite_songs});

  FavoriteSongList.fromJson(Map<String, dynamic> json) {
    favorite_songs = json['favorite_songs'] != null
        ? new SongList.fromJson(json['favorite_songs'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.favorite_songs != null) {
      data['favorite_songs'] = this.favorite_songs.toJson();
    }
    return data;
  }
}
