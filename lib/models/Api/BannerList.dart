import 'package:karani/models/Banner.dart';

class BannerList {
  List<Banner> banners;

  BannerList({this.banners});

  BannerList.fromJson(Map<String, dynamic> json) {
    if (json['banners'] != null) {
      banners = new List<Banner>();
      json['banners'].forEach((v) {
        banners.add(new Banner.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.banners != null) {
      data['banners'] = this.banners.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
