import 'package:karani/models/Category.dart';
import 'package:karani/models/SongList.dart';

class CategorySongList {
  Category category;
  SongList songs;

  CategorySongList({this.category, this.songs});

  CategorySongList.fromJson(Map<String, dynamic> json) {
    category = json['category'] != null
        ? new Category.fromJson(json['category'])
        : null;
    songs = json['songs'] != null ? new SongList.fromJson(json['songs']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.category != null) {
      data['category'] = this.category.toJson();
    }
    if (this.songs != null) {
      data['songs'] = this.songs.toJson();
    }
    return data;
  }
}
