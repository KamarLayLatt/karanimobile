import 'package:karani/models/SongList.dart';

class AllSongList {
  SongList songs;

  AllSongList({this.songs});

  AllSongList.fromJson(Map<String, dynamic> json) {
    songs = json['songs'] != null ? new SongList.fromJson(json['songs']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.songs != null) {
      data['songs'] = this.songs.toJson();
    }
    return data;
  }
}
