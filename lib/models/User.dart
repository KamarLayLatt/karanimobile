class User {
  int id;
  String uuid;
  String phone;
  String name;
  String gender;
  String title;
  String profile;
  String coverPhoto;
  Null address;
  Null deletedAt;
  String createdAt;
  String updatedAt;
  String coverPhotoPath;
  String profilePath;

  User(
      {this.id,
      this.uuid,
      this.phone,
      this.name,
      this.gender,
      this.title,
      this.profile,
      this.coverPhoto,
      this.address,
      this.deletedAt,
      this.createdAt,
      this.updatedAt,
      this.coverPhotoPath,
      this.profilePath});

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    uuid = json['uuid'];
    phone = json['phone'];
    name = json['name'];
    gender = json['gender'];
    title = json['title'];
    profile = json['profile'];
    coverPhoto = json['cover_photo'];
    address = json['address'];
    deletedAt = json['deleted_at'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    coverPhotoPath = json['cover_photo_path'];
    profilePath = json['profile_path'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['uuid'] = this.uuid;
    data['phone'] = this.phone;
    data['name'] = this.name;
    data['gender'] = this.gender;
    data['title'] = this.title;
    data['profile'] = this.profile;
    data['cover_photo'] = this.coverPhoto;
    data['address'] = this.address;
    data['deleted_at'] = this.deletedAt;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['cover_photo_path'] = this.coverPhotoPath;
    data['profile_path'] = this.profilePath;
    return data;
  }
}
