import 'package:karani/models/Category.dart';

class Data {
  int id;
  String code;
  String title;
  String slug;
  List<Category> categories;
  String lyrics;
  int isFavorite;

  Data(
      {this.id,
      this.code,
      this.title,
      this.slug,
      this.categories,
      this.lyrics,
      this.isFavorite});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    code = json['code'];
    title = json['title'];
    slug = json['slug'];
    if (json['categories'] != null) {
      categories = new List<Category>();
      json['categories'].forEach((v) {
        categories.add(new Category.fromJson(v));
      });
    }
    lyrics = json['lyrics'];
    isFavorite = json['is_favorite'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['code'] = this.code;
    data['title'] = this.title;
    data['slug'] = this.slug;
    if (this.categories != null) {
      data['categories'] = this.categories.map((v) => v.toJson()).toList();
    }
    data['lyrics'] = this.lyrics;
    data['is_favorite'] = this.isFavorite;
    return data;
  }
}
