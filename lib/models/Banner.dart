class Banner {
  int id;
  Null title;
  Null description;
  String imagePath;

  Banner({this.id, this.title, this.description, this.imagePath});

  Banner.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    description = json['description'];
    imagePath = json['image_path'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['description'] = this.description;
    data['image_path'] = this.imagePath;
    return data;
  }
}
